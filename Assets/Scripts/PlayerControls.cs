﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private float screenCenterX;
    private float screenCenterY;

    public KeyCode moveUp = KeyCode.W;
    public KeyCode moveDown = KeyCode.S;
    public float speed = 3.0f;
    public float boundY = 5f;
    private Rigidbody2D body;

    private AudioSource paddleSound;

    // Start is called before the first frame update
    void Start()
    {
        screenCenterX = Screen.width * 0.5f;
        screenCenterY = Screen.height * 0.5f;

        body = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        var vel = body.velocity;

        if(Input.touchCount > 0)
        {
            Touch firstTouch = Input.GetTouch(0);
 
            Debug.Log("touch: " + firstTouch.position.y + " phase " + firstTouch.phase);

            // if it began this frame
            if(firstTouch.phase == TouchPhase.Began)
            {
                if(firstTouch.position.y > screenCenterY)
                {   
                     vel.y = -speed;

                } else if(firstTouch.position.y < screenCenterY)
                {
                    //down
                     vel.y = speed;
                }
            } else if (firstTouch.phase == TouchPhase.Ended) {
                vel.y = 0;
            }
        }

        // if (Input.GetKey(moveUp)) {
        //     vel.y = speed;
        // } else if (Input.GetKey(moveDown)) {
        //     vel.y = -speed;
        // } else {
        //     vel.y = 0;
        // }

        body.velocity = vel;

        MovePaddle(vel);
    }

    void MovePaddle(Vector2 vector) {
        //Debug.Log("MovePaddle: " + vector);

        body.velocity = vector;

        var pos = transform.position;
        if (pos.y > boundY) {
            pos.y = boundY;
        } else if (pos.y < -boundY) {
            pos.y = -boundY;
        }
        transform.position = pos;   
    }

    void Movement() {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");
        bool vup = Input.GetKey(moveUp);
        Debug.Log("v :" + v);
        Debug.Log("u :"+vup);

        Debug.Log("h :"+h);

        Vector2 velocity = new Vector2( body.velocity.x, Vector2.right.y * speed * v);
        
        body.velocity = velocity;

        var pos = transform.position;

        if (pos.y > boundY) {
            pos.y = boundY;
        } else if (pos.y < -boundY) {
            pos.y = -boundY;
        }
        transform.position = pos;     
    }
}
